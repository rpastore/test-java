package org.asante.test;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class InvertedIndex {
	
	private Hashtable<String,ArrayList<Integer>> index;
	
	public InvertedIndex(String[] data) {
		index = new Hashtable<String,ArrayList<Integer>>();
		loadData(data);		
	}
	
	private void loadData(String[] data) {
		for(int i = 0; i < data.length; i++) {
			String frase = data[i];
			StringTokenizer tokenizer = new StringTokenizer(frase,", '-");
			while(tokenizer.hasMoreElements())
				this.put(tokenizer.nextToken(), i);
		}		
	}
	
	
	public void put(String word, int frase) {
		ArrayList<Integer> frases = index.get(word);
		if(frases == null) {
			frases = new ArrayList<Integer>();
			frases.add(frase);
			index.put(word, frases);
		}
		else {
			frases.add(frase);
			index.put(word, frases);
		}
	}
	
	public Integer[] get(String word) {
		return index.get(word).toArray(new Integer[0]);
	}

}
